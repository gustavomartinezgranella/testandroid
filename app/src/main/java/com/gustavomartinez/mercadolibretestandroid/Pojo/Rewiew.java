package com.gustavomartinez.mercadolibretestandroid.Pojo;

public class Rewiew {

    private float rating_average;
    private long total;

    public Rewiew() {}

    public Rewiew(float rating_average, long total) {
        this.rating_average = rating_average;
        this.total = total;
    }

    public float getRating_average() {
        return rating_average;
    }

    public void setRating_average(float rating_average) {
        this.rating_average = rating_average;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
