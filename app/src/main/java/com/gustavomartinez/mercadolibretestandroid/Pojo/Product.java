package com.gustavomartinez.mercadolibretestandroid.Pojo;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Product {

    private String id;
    private String site_id;
    private String title;
    private float price;
    private String currency_id;
    private long available_quantity;
    private String thumbnail;
    private Rewiew reviews;


    public Product(String id, String site_id, String title, float price, String currency_id
            , long available_quantity, String thumbail, Rewiew reviews) {
        this.id = id;
        this.site_id = site_id;
        this.title = title;
        this.price = price;
        this.currency_id = currency_id;
        this.available_quantity = available_quantity;
        this.thumbnail = thumbail;
        this.reviews = reviews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(String currency_id) {
        this.currency_id = currency_id;
    }

    public long getAvailable_quantity() {
        return available_quantity;
    }

    public void setAvailable_quantity(long available_quantity) {
        this.available_quantity = available_quantity;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbail) {
        this.thumbnail = thumbail;
    }

    public Rewiew getReviews() {
        return reviews;
    }

    public void setReviews(Rewiew reviews) {
        this.reviews = reviews;
    }

    public static Rewiew parserJSON(String response){
        Gson gson = new GsonBuilder().create();
        Rewiew rewiew = gson.fromJson(response, Rewiew.class);
        return rewiew;
    }
}
