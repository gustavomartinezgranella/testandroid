package com.gustavomartinez.mercadolibretestandroid.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.widget.Toast;
import com.gustavomartinez.mercadolibretestandroid.R;
import com.gustavomartinez.mercadolibretestandroid.Utilities.PermissionCodes;
import com.gustavomartinez.mercadolibretestandroid.Utilities.TextResources;

public class SearchActivity extends AppCompatActivity {

    private SearchView searchView;
    private String queryProduct="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchView = findViewById(R.id.sv_products);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                
                if (query.isEmpty()){
                    Toast.makeText(SearchActivity.this, TextResources.PRODUCT_NOT_ENTERED, Toast.LENGTH_SHORT).show();
                }else {
                    queryProduct = query;
                    queryProduct.replace(" ","+");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.INTERNET}, PermissionCodes.PERMISSION_INTERNET);

                    }else {
                        processForVersionOlder();
                    }    
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }


    /*
    * process action for older android's version
    * */
    private void processForVersionOlder(){
        if (CheckPermission(Manifest.permission.INTERNET)){
            Intent intentListProduct = new Intent(SearchActivity.this, ListProductActivity.class);
            intentListProduct.putExtra("queryProduct",queryProduct);
            startActivity(intentListProduct);
        }else{
            Toast.makeText(this, TextResources.CANCEL_PERMISSION, Toast.LENGTH_SHORT).show();
        }
    }


    /*
    *
    * Verify if permission has accepted
    *
    * @Param permission
    * @return boolean
    *
    * */
    private boolean CheckPermission(String permission){
        int result = checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PermissionCodes.PERMISSION_INTERNET :
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.INTERNET)){
                    //verify if acept or cancel permission's request
                    if(result==PackageManager.PERMISSION_GRANTED){
                        Intent intentListProduct = new Intent(SearchActivity.this, ListProductActivity.class);
                        intentListProduct.putExtra("queryProduct",queryProduct.replaceAll(" ","+"));
                        startActivity(intentListProduct);
                    }else{
                        //permission dened
                        Toast.makeText(SearchActivity.this, TextResources.CANCEL_PERMISSION, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + getPackageName()));

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }
}
