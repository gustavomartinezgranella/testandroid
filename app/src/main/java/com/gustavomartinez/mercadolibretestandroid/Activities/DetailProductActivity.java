package com.gustavomartinez.mercadolibretestandroid.Activities;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.gustavomartinez.mercadolibretestandroid.R;
import com.squareup.picasso.Picasso;

import java.io.File;

public class DetailProductActivity extends AppCompatActivity {

    private TextView code;
    private TextView tittle;
    private TextView price;
    private TextView quantity;
    private RatingBar ratingBar;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getActionBar().setDisplayShowHomeEnabled(true);
        }

        code = findViewById(R.id.tv_code);
        tittle = findViewById(R.id.tv_detail_product_title);
        price = findViewById(R.id.tv_detail_product_price);
        quantity = findViewById(R.id.tv_detail_product_available);
        ratingBar = findViewById(R.id.ratingBar);
        image = findViewById(R.id.picture_product);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            code.setText("Codigo : "+extras.getString("code"));
            tittle.setText(extras.getString("tittle"));
            quantity.setText(String.valueOf("Unidades disponibles : "+extras.getLong("quantity")));
            price.setText("Precio : "+extras.getString("price"));
            ratingBar.setNumStars(6);
            ratingBar.setRating(extras.getFloat("rating"));

            try {
                String url = extras.getString("thumbnail");
                Picasso.get().load(url)
                        .into(image);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }
}
