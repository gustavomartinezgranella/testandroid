package com.gustavomartinez.mercadolibretestandroid.Activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gustavomartinez.mercadolibretestandroid.Adapters.ProductAdapter;
import com.gustavomartinez.mercadolibretestandroid.Pojo.Product;
import com.gustavomartinez.mercadolibretestandroid.R;
import com.gustavomartinez.mercadolibretestandroid.Utilities.HTTPAsyncRequest;
import com.gustavomartinez.mercadolibretestandroid.Utilities.TextResources;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ListProductActivity extends AppCompatActivity {

    private ListView listView;
    private List<Product> products = new ArrayList<>();
    private ProductAdapter productAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else{
            getActionBar().setDisplayShowHomeEnabled(true);
        }

        try {
            listView = findViewById(R.id.list_view);
            products = requestProducts();

            if(products.size() == 0){
                Toast.makeText(ListProductActivity.this, TextResources.NOT_RESULT, Toast.LENGTH_SHORT).show();
            }else {
                productAdapter = new ProductAdapter(ListProductActivity.this, R.layout.list_view_products, products);
                listView.setAdapter(productAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intentDetail = new Intent(ListProductActivity.this, DetailProductActivity.class);

                        Bundle extras = getIntent().getExtras();
                        if(extras != null) {
                            intentDetail.putExtra("queryProduct", extras.getString("queryProduct"));
                        }


                        intentDetail.putExtra("code", products.get(position).getId());
                        intentDetail.putExtra("tittle", products.get(position).getTitle());
                        intentDetail.putExtra("thumbnail", products.get(position).getThumbnail());
                        intentDetail.putExtra("quantity", products.get(position).getAvailable_quantity());
                        intentDetail.putExtra("price", "$"+products.get(position).getPrice());
                        intentDetail.putExtra("rating", products.get(position).getReviews().getRating_average());

                        startActivity(intentDetail);
                    }
                });
                registerForContextMenu(listView);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
    * this method get the products obtain from mercadolibre api
    * */
    public List<Product> requestProducts(){

        String query = "";

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            query = extras.getString("queryProduct");
        }

        StringBuilder urlApiML = new StringBuilder("https://api.mercadolibre.com/sites/MLA/search?q=")
                .append(query);
        HTTPAsyncRequest getRequest = new HTTPAsyncRequest();

        JSONObject jsonObj = null;
        JSONArray jsonArray = null;

        List<Product> products = new ArrayList<>();
        try {
            String productsRequested = getRequest.execute(urlApiML.toString()).get();
            if(productsRequested!=null){

                if (productsRequested.isEmpty()){
                    Toast.makeText(this, TextResources.NOT_RESULT, Toast.LENGTH_SHORT).show();
                }else {
                    try {
                        jsonObj = new JSONObject(productsRequested);
                        jsonArray = (JSONArray) jsonObj.get("results");
                        Gson gson = new GsonBuilder().create();
                        for(int index=0; index<jsonArray.length(); index++){
                            if ( jsonArray.get(index) instanceof JSONObject ) {
                                products.add(gson.fromJson(jsonArray.get(index).toString(), Product.class));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return products;
    }

    //metodo que se sobreescribe para bloquear el boton atras
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return (keyCode == KeyEvent.KEYCODE_BACK || super.onKeyDown(keyCode, event));
    }


}
