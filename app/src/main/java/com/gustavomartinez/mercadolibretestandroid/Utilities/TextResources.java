package com.gustavomartinez.mercadolibretestandroid.Utilities;

public class TextResources {

    public static final String CANCEL_PERMISSION = "La solicitud de permiso fue cancelada";
    public static final String PRODUCT_NOT_ENTERED = "Ingrese el producto a buscar...";
    public static final String NOT_RESULT = "No se encontraron resultados para el producto ingresado";
    public static final String NOT_CONNECTION_AVAILABLE = "No tienes una conexion a internet...";
}
