package com.gustavomartinez.mercadolibretestandroid.Utilities;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPAsyncRequest extends AsyncTask<String, Void, String>{

    private static final String REQUEST_TYPE_GET = "GET";
    private static final int READ_TIMEOUT = 15000;
    private static final int CONNECTION_TIMEOUT = 15000;


    @Override
    protected String doInBackground(String... strings) {
        String url = strings[0];
        String products;
        String inputLine;

        try {
            URL myUrl = new URL(url);

            //Create a connection
            HttpURLConnection connection =(HttpURLConnection)
                    myUrl.openConnection();

            connection.setRequestMethod(REQUEST_TYPE_GET);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);

            connection.connect();

            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Check if the line we are reading is not null
            while((inputLine = reader.readLine()) != null){
                stringBuilder.append(inputLine);
            }

            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();

            //Set our result equal to our stringBuilder
            products = stringBuilder.toString();
        }catch(IOException e){
            e.printStackTrace();
            products = null;
        }
        return products;
    }

    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);
    }
}
