package com.gustavomartinez.mercadolibretestandroid.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gustavomartinez.mercadolibretestandroid.Pojo.Product;
import com.gustavomartinez.mercadolibretestandroid.R;

import java.util.List;

public class ProductAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Product> productList;

    public ProductAdapter(Context context, int layout, List<Product> products){
        this.context = context;
        this.productList = products;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return this.productList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.productList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(this.productList.get(i).getId().substring(3));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        try {
            ViewHolder viewHolder;

            if(convertView == null){
                //inflo la vista con mi layout personalizado
                LayoutInflater layoutInflater = LayoutInflater.from(this.context);
                convertView = layoutInflater.inflate(this.layout, null);
                viewHolder = new ViewHolder();

                viewHolder.title = convertView.findViewById(R.id.title_product);
                viewHolder.reputation = convertView.findViewById(R.id.rb_reputation);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            //nos traemos el producto actual
            Product product = this.productList.get(position);

            //seteo el viewHolder
            viewHolder.title.setText(product.getTitle());
            viewHolder.reputation.setNumStars(6);
            viewHolder.reputation.setRating(product.getReviews().getRating_average());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;

    }

    static class ViewHolder{
        private TextView title;
        private RatingBar reputation;
    }
}
